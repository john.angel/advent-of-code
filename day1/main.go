package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"unicode"
)

func readLinesFromFile() []string {
	file, err := os.Open("day1/exampleinput")
	defer file.Close()

	if err != nil {
		log.Fatal("An error occurred while reading file")
	}
	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
		//fmt.Printf("%s\n", scanner.Text())
	}

	return lines
}

var numWords = map[string]int{
	"one":   1,
	"two":   2,
	"three": 3,
	"four":  4,
	"five":  5,
	"six":   6,
	"seven": 7,
	"eight": 8,
	"nine":  9,
}

func parseSpelledNumber(input string, num *int) bool {
	fmt.Printf("input was: %s\n", input)
	for key, value := range numWords {
		if input == key {
			num = &value
			return true
		}
	}
	return false
}

func main() {
	lines := readLinesFromFile()
	sum := 0
	for _, line := range lines {
		fmt.Printf("%s\n", line)

		left := -1
		right := -1

		leftPos := 0
		rightPos := len(line) - 1

		for j := 0; j < len(line); j++ {
			if unicode.IsDigit(rune(line[j])) {
				if left == -1 {
					left = int(line[j] - '0')
					right = int(line[j] - '0')

					leftPos = j
					rightPos = j
				} else {
					right = int(line[j] - '0')
					rightPos = j
				}
			}

		}
		fmt.Printf("value: %d\n", 10*left+right)
		sum += 10*left + right
	}
	fmt.Printf("The total is: %d\n", sum)
}
